var http = require('http');

exports.on = function(io, twit){

  var options = {
    host: 'animemap.net',
    path: '/api/table/shizuoka.json'
  };

  var searchTweets = function(tracks) {
    io.sockets.on('connection', function (socket) {
      console.log('socket connected.');
      twit.stream('statuses/filter', {'track': tracks}, function(stream) {
        stream.on('data', function (data) {
          console.log('tweets: ' + JSON.stringify(data));
          socket.emit('send', {
            text: data.text,
            user: data.user.name
          });
        });
      });
      socket.on('disconnect', function () {
        io.sockets.emit('user disconnected');
      });
    });
  };

  var getTitles = function(response) {
    var str = '';

    response.on('data', function (chunk) {
      str += chunk;
    });

    response.on('end', function () {
      var items = JSON.parse(str).response.item;

      var titles = '';
      for (var i = 0 ; i < items.length ; i++) {
        var item = items[i];
        var title = item.title;
        if(title === 'undefined') {
          continue;
        } else if(titles !== null && titles.length != 0) {
          titles += ', ';
        }
        titles += title;
      }

      console.log('titles: ' + titles);
      searchTweets(titles);
    });
  };

  http.request(options, getTitles).end();
};
