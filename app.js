var express = require('express'),
    http = require('http'),
    fs = require('fs'),
    config = require('./config/config');

// mongoose.connect(config.db);
// var db = mongoose.connection;
// db.on('error', function () {
//   throw new Error('unable to connect to database at ' + config.db);
// });
//
// var modelsPath = __dirname + '/app/models';
// fs.readdirSync(modelsPath).forEach(function (file) {
//   if (file.indexOf('.js') >= 0) {
//     require(modelsPath + '/' + file);
//   }
// });

var app = express(),
    server = http.createServer(app),
    io = require('socket.io').listen(server);

// routes
require('./config/express')(app, config);
require('./config/routes')(app);

server.listen(config.port);

// anime apiから取得したアニメ一覧をキーにtwitter検索
var twit = require('./config/twitter');
var animeTweetSearcher = require('./app/controllers/animeTweetSearcher');
animeTweetSearcher.on(io, twit);

